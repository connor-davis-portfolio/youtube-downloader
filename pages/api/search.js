import ytsr from "ytsr";

const handler = (request, response) => {
  try {
    const text = request.query.text;

    response.setHeader("Access-Control-Allow-Credentials", "true")
    response.setHeader("Access-Control-Allow-Origin", "*")
    response.setHeader("Access-Control-Allow-Methods", "GET,OPTIONS")
    response.setHeader(
      "Access-Control-Allow-Headers",
      "X-CSRF-Token, X-requestuested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version"
    )

    ytsr(text, async (error, result) => {
        if (error) throw new Error(error);

        if (result.items) return response.json(result.items.filter((item) => item.type === "video"));

        return response.send("EMPTY");
    });
    return;
  } catch (e) {
    return;
  }
}

module.exports = handler