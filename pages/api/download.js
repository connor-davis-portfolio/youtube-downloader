import ytdl from "ytdl-core";

const handler = (request, response) => {
  try {
    const url = "https://www.youtube.com/watch?v=" + request.query.id;
    
    response.setHeader("Access-Control-Allow-Credentials", "true")
    response.setHeader("Access-Control-Allow-Origin", "*")
    response.setHeader("Access-Control-Allow-Methods", "GET,OPTIONS")
    response.setHeader(
      "Access-Control-Allow-Headers",
      "X-CSRF-Token, X-requestuested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version"
    )

    ytdl(url, {
      filter: (video) => video.container === "mp4" && video.hasAudio && video.hasVideo
    })
      .on("response", res => {
        response.setHeader("Content-Length", res.headers["content-length"]);
      })
      .pipe(response);
    return;
  } catch (e) {
    return;
  }
}

module.exports = handler