import React, { Component } from 'react';
import { Container } from 'react-bootstrap';
import ytdl from "ytdl-core";
import { saveAs } from "file-saver";
import axios from "axios";
import $ from "jquery";
import { Row, Col } from "react-bootstrap";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchQuery: "",
            searchResults: [],
            searching: false,
            downloading: false,
            downloadPercentage: 0
        };

        this.handleSearchQueryChange = this.handleSearchQueryChange.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
    }

    handleSearchQueryChange(textValue) {
        this.setState({
            searchQuery: textValue.target.value
        })
    }

    async handleSearch() {
        const searchQuery = this.state.searchQuery;

        this.setState({
            searching: true
        });

        fetch("/api/search?text=" + searchQuery)
            .then(async response => {
                const results = await response.json();


                this.setState({
                    searching: false,
                    searchResults: results
                });
            });
    }

    async handleDownloadVideo(result) {
        const id = result.link.match(/(?:youtu\.be\/|youtube\.com(?:\/embed\/|\/v\/|\/watch\?v=|\/user\/\S+|\/ytscreeningroom\?v=))([\w\-]{10,12})\b/)[1];

        this.setState({
            downloading: true,
            searchQuery: "",
            searchResults: []
        });

        fetch("/api/download?id=" + id)
            .then(async response => {
                const reader = response.body.getReader();

                const contentLength = +response.headers.get('Content-Length');

                let receivedLength = 0;
                let chunks = [];
                while (true) {
                    const { done, value } = await reader.read();

                    if (done) {
                        break;
                    }

                    chunks.push(value);
                    receivedLength += value.length;

                    const percentage = Math.floor((receivedLength / contentLength) * 100);

                    this.setState({
                        downloadPercentage: percentage
                    });
                }

                let chunksAll = new Uint8Array(receivedLength);
                let position = 0;
                for (let chunk of chunks) {
                    chunksAll.set(chunk, position);
                    position += chunk.length;
                }

                const file = new Blob([chunksAll], { type: "video\/mp4" });

                saveAs(file, result.title + ".mp4", { autoBom: false });

                this.setState({
                    downloading: false,
                    downloadPercentage: 0
                });
            })
    }

    render() {
        const results = this.state.searchResults;

        return (
            <div className="d-app">
                <header className="d-toolbar">
                    YouTube Downloader
                </header>

                {this.state.downloading ? <div className="d-downloading">Downloading {this.state.downloadPercentage}%</div> : <div>
                    <div className="d-searchbar">
                        <input className="d-searchbar-input" value={this.state.searchQuery} placeholder="Type..." onChange={this.handleSearchQueryChange}></input>
                        <button className="d-searchbar-button d-btn d-btn-primary" onClick={this.handleSearch}>Search</button>
                    </div>
                    {this.state.searching ?
                        <div className="d-downloading">Searching...</div> :
                        <div className="d-results" style={{overflowY: "auto"}}>
                            {results.map((result, index) => (
                                <div key={index} className="d-result">
                                    <div className="d-result-header">
                                        <img className="d-result-thumbnail" src={result.thumbnail} />
                                        <div className="d-result-title">{result.title}</div>
                                    </div>
                                    <div className="d-result-footer">
                                        <div className="d-result-downloadbutton" onClick={this.handleDownloadVideo.bind(this, result)}>Download Video</div>
                                    </div>
                                </div>)
                            )}
                        </div>}
                </div>}
            </div>
        );
    }
}

export default App;
